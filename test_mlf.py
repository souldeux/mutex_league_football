from dataclasses import dataclass

import requests
from requests import Response

import numpy as np
import matplotlib.pyplot as plt


@dataclass
class MLFResponse:
    def __init__(self, response: Response):
        _x, _y = response.text.split("\n")
        self.x = int(_x.split("=")[1])
        self.y = int(_y.split("=")[1])

    x: int
    y: int


def get_from_mlf() -> MLFResponse:
    response = requests.get("http://localhost:4500")
    return MLFResponse(response)


if __name__ == "__main__":
    responses = []
    iterations = 1000
    for _ in range(iterations):
        if (_+1) % 10 == 0:
            print(f"Iteration {_+1}")
        responses.append(get_from_mlf())

    x = np.array([i+1 for i in range(iterations)])

    X = []
    Y = []
    for r in responses:
        X.append(r.x)
        Y.append(r.y)

    X = np.array(X)
    Y = np.array(Y)

    plt.title("Divergence between locked (green) & unlocked (red) increment in Go")
    plt.xlabel("Iterations")
    plt.ylabel("Iterator Value")
    plt.plot(x, X, color="green")
    plt.plot(x, Y, color="red")
    plt.show()


