# Why Mutex Matters

### 1. Build & run the Docker image

```shell
ninja && docker run -p 4500:4500 gcr.io/policyfly/mlf:develop
```

### 2. Run the test / demo script

```shell
python3 test_mlf.py
```

### 3. Look at the pretty graph

Art. ![](Figure_1.png)

The point is more impressive if you zoom in on the end.