package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

func increment(x *int, y *int, m *sync.Mutex) {
	m.Lock()
	*x++
	m.Unlock()
	*y++
}

var globalX = 0
var globalY = 0

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		mooTex := &sync.Mutex{}
		iterations := 1000
		wg := sync.WaitGroup{}
		for i := 0; i < iterations; i++ {
			wg.Add(1)
			go func(x *int, y *int, m *sync.Mutex) {
				increment(x, y, m)
				wg.Done()
			}(&globalX, &globalY, mooTex)
		}
		wg.Wait()
		_, err := fmt.Fprintf(w, "X (locked) = %d\nY (unlocked) = %d", globalX, globalY)
		if err != nil {
			log.Fatal("how did this break wow")
		}
	})
	log.Fatal(http.ListenAndServe(":4500", nil))
}
