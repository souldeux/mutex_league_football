FROM alpine:latest as cert-builder
RUN apk --update add ca-certificates

FROM scratch
COPY --from=cert-builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ADD ./cmd/mlf/mlf /mlf
EXPOSE 4500
CMD ["/mlf"]
